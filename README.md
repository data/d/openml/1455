# OpenML dataset: acute-inflammations

https://www.openml.org/d/1455

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Jacek Czerniak     
**Source**: UCI     
**Please cite**:  J.Czerniak, H.Zarzycki, Application of rough sets in the presumptive diagnosis of urinary system diseases, Artificial Intelligence and Security in Computing Systems, ACS'2002 9th International Conference Proceedings, Kluwer Academic Publishers,2003, pp. 41-51.


* Abstract: 

The data was created by a medical expert as a data set to test the expert system, which will perform the presumptive diagnosis of two diseases of the urinary system.

* Source:   
  
Jacek Czerniak, Ph.D., Assistant Professor
Systems Research Institute
Polish Academy of Sciences
Laboratory of Intelligent Systems
ul. Newelska 6, Room 218
01-447 Warszawa, Poland
e-mail: jacek.czerniak 'at' ibspan.waw.pl or jczerniak 'at' ukw.edu.pl 


* Data Set Information:
 
The main idea of this data set is to prepare the algorithm of the expert system, which will perform the presumptive diagnosis of two diseases of urinary system. It will be the example of diagnosing of the acute inflammations of urinary bladder and acute nephritises. For better understanding of the problem let us consider definitions of both diseases given by medics. Acute inflammation of urinary bladder is characterised by sudden occurrence of pains in the abdomen region and the urination in form of constant urine pushing, micturition pains and sometimes lack of urine keeping. Temperature of the body is rising, however most often not above 38C. The excreted urine is turbid and sometimes bloody. At proper treatment, symptoms decay usually within several days. However, there is inclination to returns. At persons with acute inflammation of urinary bladder, we should expect that the illness will turn into protracted form.

Acute nephritis of renal pelvis origin occurs considerably more often at women than at men. It begins with sudden fever, which reaches, and sometimes exceeds 40C. The fever is accompanied by shivers and one- or both-side lumbar pains, which are sometimes very strong. Symptoms of acute inflammation of urinary bladder appear very often. Quite not infrequently there are nausea and vomiting and spread pains of whole abdomen.

The data was created by a medical expert as a data set to test the expert system, which will perform the presumptive diagnosis of two  diseases of urinary system. The basis for rules detection was Rough Sets Theory. Each instance represents an potential patient. Each line of the data file starts with a digit which tells the temperature of patient.
  
-- Attribute lines:   
For example, '35,9 no no yes yes yes yes no'   
Where:   
'35,9' Temperature of patient   
'no' Occurrence of nausea   
'no' Lumbar pain   
'yes' Urine pushing (continuous need for urination)   
'yes' Micturition pains   
'yes' Burning of urethra, itch, swelling of urethra outlet   
'yes' decision: Inflammation of urinary bladder   
'no' decision: Nephritis of renal pelvis origin    

*  Attribute Information:
  
a1 Temperature of patient { 35C-42C }    
a2 Occurrence of nausea { yes, no }   
a3 Lumbar pain { yes, no }   
a4 Urine pushing (continuous need for urination) { yes, no }   
a5 Micturition pains { yes, no }   
a6 Burning of urethra, itch, swelling of urethra outlet { yes, no }   
d1 decision: Inflammation of urinary bladder { yes, no }   
d2 decision: Nephritis of renal pelvis origin { yes, no }   


* Relevant Papers:

J.Czerniak, H.Zarzycki, Application of rough sets in the presumptive diagnosis of urinary system diseases, Artificial Intelligence and Security in Computing Systems, ACS'2002 9th International Conference Proceedings, Kluwer Academic Publishers,2003, pp. 41-51

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1455) of an [OpenML dataset](https://www.openml.org/d/1455). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1455/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1455/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1455/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

